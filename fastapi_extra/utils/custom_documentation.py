from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi
from fastapi.routing import APIRoute
from fastapi_extra.utils.wrapper_response import WrapperResponse
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def update_response(default_response_json, schema):
    del default_response_json['schema']
    default_response_json['schema'] = dict()
    title = schema.get('title', None)
    if title:
        default_response_json['schema']['title'] = title
        del schema['title']
    default_response_json['schema']['type'] = 'object'

def custom_docs(app: FastAPI):
    old = app.openapi

    def custom_openapi(openapi_prefix: str = ""):
        if app.openapi_schema:
            return app.openapi_schema
        try:
            openapi_schema = get_openapi(
                title=app.title,
                version=app.version,
                description=app.description,
                routes=app.routes,
                openapi_prefix=openapi_prefix
            )
            for route in app.routes:
                if not isinstance(route, APIRoute):
                    continue
                if route.response_class is WrapperResponse:
                    default_response_json = openapi_schema['paths'][route.path_format][str.lower(next(iter(route.methods)))]['responses'][
                        str(route.status_code)][
                        'content']['application/json']
                    schema = default_response_json['schema'].copy()
                    update_response(default_response_json, schema)
                    default_response_json['schema']['properties'] = {"result": schema}
                r = openapi_schema['paths'][route.path_format][str.lower(next(iter(route.methods)))]['responses']
                responses = []
                for key in r.keys():
                    if int(key) in range(400, 600):
                        responses.append(key)
                for response in responses:
                    default_response_json = openapi_schema['paths'][route.path_format][str.lower(next(iter(route.methods)))]['responses'][response][
                        'content']['application/json']
                    schema = default_response_json['schema'].copy()
                    update_response(default_response_json, schema)
                    if response == '422':
                        default_response_json['schema']['properties'] = {"error": {"type": "object", "properties": {
                            "code": {"type": "integer", "format": "int32", "default": int(response)},
                            "message": {"type": "string"}, "detail": {'$ref': '#/components/schemas/ValidationError'}}}}
                    else:
                        default_response_json['schema']['properties'] = {"error": {"type": "object", "properties": {
                            "code": {"type": "integer", "format": "int32", "default": int(response)},
                            "message": {"type": "string"}}}}
        except Exception as e:
            logger.error(f"The following error was caught while updating the openapi docs: {str(e)} ")
            openapi_schema = old(openapi_prefix)
        app.openapi_schema = openapi_schema
        return app.openapi_schema

    app.openapi = custom_openapi
