from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.openapi.utils import get_openapi
from mongoengine import NotRegistered, InvalidDocumentError, LookUpError, DoesNotExist, MultipleObjectsReturned, \
    InvalidQueryError, OperationError, NotUniqueError, BulkWriteError, FieldDoesNotExist, ValidationError, \
    SaveConditionError, DeprecatedError
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse

mongoengine_errors = (
    NotRegistered,
    InvalidDocumentError,
    LookUpError,
    DoesNotExist,
    MultipleObjectsReturned,
    InvalidQueryError,
    OperationError,
    NotUniqueError,
    BulkWriteError,
    FieldDoesNotExist,
    ValidationError,
    SaveConditionError,
    DeprecatedError,
)


async def request_validation_exception_handler(
        request: Request, exc: RequestValidationError
) -> JSONResponse:
    error = jsonable_encoder(exc.errors())
    response = {"error": {"code": 422, "message": "Entity validation error", "detail": error}}
    return JSONResponse(status_code=422, content=response)


async def custom_http_exception_handler(request: Request, exc: HTTPException) -> JSONResponse:
    headers = getattr(exc, "headers", None)
    response = {"error": {"code": exc.status_code, "message": exc.detail}}
    return JSONResponse(content=response, status_code=exc.status_code, headers=headers)



async def custom_exception_handler(request, exc):
    response = {
        "error": {"code": 500, "message": f"{exc.__class__.__name__}",
                  "detail": "Check the server log there are unhandled exceptions"}}
    if isinstance(exc, mongoengine_errors):
        response['error']['detail'] = str(exc)
    return JSONResponse(content=response, status_code=500)


def change_exception_handling(app: FastAPI):
    app.add_exception_handler(HTTPException, custom_http_exception_handler)
    app.add_exception_handler(Exception, custom_exception_handler)
    app.add_exception_handler(RequestValidationError, request_validation_exception_handler)
