from pydantic import BaseSettings


class CertificateSettings(BaseSettings):
    PRIVATE_KEY_FILE: str = None
    CERTIFICATE_FILE: str = None
    CA_FILE: str = None


certificateSettings = CertificateSettings()


class HttpClientSettings(BaseSettings):
    AIOHTTP_POOL_SIZE: int = 100
    AIOHTTP_TIMEOUT: int = 86400  # timeout 24h

    AIOHTTP_USE_SSL: bool = False


httpClientSettings = HttpClientSettings()



