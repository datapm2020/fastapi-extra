from fastapi import FastAPI

from fastapi_extra.utils.custom_documentation import custom_docs
from fastapi_extra.utils.custom_exception_handler import change_exception_handling
from fastapi_extra.utils.wrapper_response import WrapperResponse


def customize_fastapi_responses(app: FastAPI):
    app.default_response_class = WrapperResponse
    change_exception_handling(app)
    custom_docs(app)
