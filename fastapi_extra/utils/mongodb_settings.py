import os

from pydantic import BaseSettings


class MongodbSettings(BaseSettings):
    MONGODB_SERVER: str
    MONGODB_USER: str
    MONGODB_PASSWORD: str

    class Config:
        case_sensitive = True


if os.path.isdir('/secrets'):
    mongodb_settings = MongodbSettings(_secrets_dir="/secrets")
else:
    mongodb_settings = MongodbSettings()
