import json
from starlette.responses import JSONResponse
from pydantic import typing

from fastapi_extra.schemas.response_wrapper import ResponseWrapper


class WrapperResponse(JSONResponse):
    media_type = "application/json"

    def render(self, content: typing.Any) -> bytes:
        response = {}
        if not isinstance(content, ResponseWrapper):
            response['result'] = content
        else:
            response = content
        return json.dumps(
            response,
            ensure_ascii=False,
            allow_nan=False,
            indent=None,
            separators=(",", ":"),
        ).encode("utf-8")
