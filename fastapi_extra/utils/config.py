from pydantic import BaseSettings


class Settings(BaseSettings):
    EXPIRATION_TIME: int = 604800


settings = Settings()
