from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, Union

from fastapi import Depends
from pydantic import BaseModel, Extra

from mongoengine import Document
from pydantic.generics import GenericModel

ModelType = TypeVar("ModelType", bound=Document)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class Repository(GenericModel, Generic[ModelType]):
    document: Type[ModelType]

    class Config:
        extra = Extra.allow

    def __init__(self, document: Type[ModelType] = None):
        """
        CRUD object with default methods to Create, Read, Update, Delete (CRUD).

        **Parameters**

        * `document`: A mongoengine Document class
        * `schema`: A Pydantic model (schema) class
        """
        if document is None:
            document = self.__fields__["document"].outer_type_.__args__[0]
        super().__init__(document=document)
        self.objects = self.document.objects

    def getById(self, _id: str) -> Optional[ModelType]:
        return self.objects(id=_id).get()

    def getByIds(self, _ids: List[str]):
        return self.objects(id__in=_ids)

    def get_all(
            self, skip: int = 0, limit: int = 100):
        #  -> List[ModelType]:
        return self.objects[skip:limit]

    def create(self, obj_in: Union[CreateSchemaType, Dict[str, Any]]) -> ModelType:
        db_obj = self.document(**obj_in)  # type: ignore
        db_obj.save()
        return db_obj

    def update(
            self,
            db_obj: ModelType,
            obj_in: Union[UpdateSchemaType, Dict[str, Any]]
    ) -> ModelType:
        if not isinstance(db_obj, self.document):
            raise Exception("object to update have different type")

        db_obj.update(**obj_in)
        return db_obj

    def remove(self, _id: str) -> ModelType:
        obj: ModelType
        obj = self.objects(id=_id).get()
        obj.delete()
        return obj
