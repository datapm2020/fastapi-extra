from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from fastapi import Request
import logging

from fastapi_extra.schemas.certificate import Certificate

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


async def patch():
    from uvicorn.protocols.http.httptools_impl import HttpToolsProtocol
    old_on_url = HttpToolsProtocol.on_url

    def new_on_url(self, url):
        old_on_url(self, url)
        self.scope['transport'] = self.transport

    HttpToolsProtocol.on_url = new_on_url


patch()


class MutualTLSMiddleware(BaseHTTPMiddleware):
    async def dispatch(
            self, request: Request, call_next: RequestResponseEndpoint
    ):

        peer_cert = request.scope['transport'].get_extra_info('peercert')
        if peer_cert:
            certificate = Certificate()
            for key, value in peer_cert["issuer"]:
                if key == "commonName":
                    certificate.issuer = value
            for key, value in peer_cert["subject"]:
                if key == "commonName":
                    certificate.subject = value
            request.state.peer_cert = certificate
            logger.info(
                f"Certificate:subject={certificate.subject}, issuer: {certificate.issuer}")
        else:
            request.state.peer_cert = None

        response = await call_next(request)

        return response
