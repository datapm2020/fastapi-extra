import datetime

from mongoengine import DateField, DateTimeField, queryset_manager

from fastapi_extra.utils.config import settings


class SoftDeleteDocument:
    creation_date = DateField(db_field="CreationDate")
    modification_date = DateField(db_field="ModificationDate")
    deleted_date = DateTimeField(db_field="DeletedDate")
    meta = {
        'indexes': [
            {'fields': ['deleted_date'], 'expireAfterSeconds': settings.EXPIRATION_TIME}
        ]
    }

    @queryset_manager
    def objects(doc_cls, queryset):
        return queryset.filter(deleted_date=None)

    @queryset_manager
    def objects_all(doc_cls, queryset):
        return queryset

    def save(self, *args, **kwargs):
        if not self.creation_date:
            self.creation_date = datetime.date.today()
        self.modification_date = datetime.date.today()
        return super(SoftDeleteDocument, self).save(*args, **kwargs)

    def update(self, *args, **kwargs):
        self.modification_date = datetime.date.today()
        return super(SoftDeleteDocument, self).save(*args, **kwargs)
