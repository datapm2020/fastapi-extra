from pydantic.main import BaseModel


class Certificate(BaseModel):
    subject: str = None
    issuer: str = None