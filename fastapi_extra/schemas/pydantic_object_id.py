from pydantic import BaseModel


class PydanticObjectId(BaseModel):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        try:
            return str(v)
        except Exception as err:
            raise TypeError(f"ObjectId required: {err}")

    class Config:
        orm_mode = True
        schema_extra = {
            "example": "ObjectId"
        }
